#include "rotate.h"
#include "image.h"
#include <malloc.h>
#include <stddef.h>
void rotate(struct image* source, struct image* rotate){
    uint32_t width = source->height;
    uint32_t height = source->width;
    create_image(rotate, width, height);

    for (uint64_t i = 0; i < height; i++)
    {
        for (uint64_t j = 0; j < width; j++)
        {
            set_pixel(rotate, width - j - 1, i, get_pixel(source, i, j));
        }
    }
}
