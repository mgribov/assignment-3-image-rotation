#include "image.h"
#include "bmp.h"
#include <malloc.h>



void create_image(struct image * img, uint64_t width, uint64_t height) {
    img->width = width;
    img->height = height;
    uint64_t pad = padding(width);
    img->data = malloc(width * height * sizeof(struct pixel) + pad);
    if (img->data == 0) {
        fprintf(stderr, "Аут оф мемори");
    }
}

struct pixel get_pixel(struct image* img, uint64_t x, uint64_t y) {
    return img->data[img->width * y + x];
}

void set_pixel(struct image *img, uint64_t x, uint64_t y, struct pixel pixel) {
    img->data[img->width * y + x] = pixel;
}


void clear(struct image* img){
    free(img->data);
    img->data = NULL;
}

