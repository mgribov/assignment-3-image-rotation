#include "bmp.h"
#include "rotate.h"
#include <stdlib.h>
//#include "image.h"

int main( int argc, char** argv ) {
    
    if(argc<3){
        printf("Вы ввели недостаточно аргументов");
        printf("\n");
        return 1;
    }
    FILE *file = fopen(argv[1], "rb");
    if (file == NULL){
            printf("Файл, который вы хотите открыть, не существует или заблокирован.");
            printf("\n");
            return 1;
        }
    struct image img = {0};
    struct image rotated = {0};
    enum read_status status = from_bmp(file, &img);
    if(status!=READ_OK){
        printf("Ошибка при чтении. Статус: ");
        char * st = unpack_read_status(status);
        printf("%s", st);
        printf("\n");
        clear(&img);
        clear(&rotated);
        fclose(file);
        return 1;
    }

    rotate(&img, &rotated);

    FILE *file_w = fopen(argv[2], "wb");
    if (file_w == NULL){
            printf("Файл, который вы хотите создать, не может быть создан.");
            printf("\n");
            clear(&img);
            clear(&rotated);
            return 1;
        }
    enum write_status w_status = to_bmp(file_w, &rotated);
    if(w_status!=WRITE_OK){
        printf("Произошла ошибка при записи файла ");
        printf("\n");
        clear(&img);
        clear(&rotated);
        fclose(file_w);
        return 1;
    }
    clear(&img);
    clear(&rotated);
    return 0;
}
