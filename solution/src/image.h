#pragma once
#include <stdint.h>

struct pixel { uint8_t b, g, r; };

struct image {
  uint64_t width, height;
  struct pixel* data;
};


void create_image(struct image* img, uint64_t width, uint64_t height);

void set_pixel(struct image* img, uint64_t x, uint64_t y, struct pixel pixel);

struct pixel get_pixel(struct image* img, uint64_t x, uint64_t y);

void clear(struct image* img);
