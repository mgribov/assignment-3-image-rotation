#include "bmp.h"
#include "service.h"

char *unpack_read_status(enum read_status status)
{
    switch (status)
    {
    case READ_INVALID_SIGNATURE:
        return "Указана неверная сигнатура файла";
        break;
    case READ_INVALID_BITS:
    case READ_PLANES_UNSUPPORTED:
    case READ_COMPRESSION_UNSUPPORTED:
        return "Указанный файл не поддерживается в данной версии программы";
        break;
    case READ_PIXELS_ERROR:
        return "Ошибка при расшифровке пикселей";
        break;
    case READ_INVALID_HEADER:
        return "Заголовок файла повреждён или отсутствует";
        break;
    default:
        return "Неизвестная ошибка";
    }
}

enum read_status check_bmp_header(struct bmp_header *header)
{
    if (header->bfType != TYPE)
    {
        return READ_INVALID_SIGNATURE;
    }
    else if (header->biPlanes != PLANES)
    {
        return READ_PLANES_UNSUPPORTED;
    }
    else if (header->biCompression != COMPRESSION)
    {
        return READ_COMPRESSION_UNSUPPORTED;
    }
    else if (header->biBitCount != BITCOUNT)
    {
        return READ_BITCOUNT_ERROR;
    }
    else
    {
        return READ_OK;
    }
}

uint64_t padding(uint64_t x)
{
    if (x % 4 == 0)
        return 0;
    return 4 - (x * 3) % 4;
}

void make_header_to_write(struct image const *img, struct bmp_header *header_to_write)
{
    uint64_t x = img->width;
    uint64_t y = img->height;
    uint64_t pad_to_write = padding(x);
    header_to_write->bfileSize = sizeof(struct bmp_header) + (x * 3 + pad_to_write) * img->height;
    header_to_write->bfileSize = 0;
    header_to_write->bfType = TYPE;
    header_to_write->bfReserved = UNKNOWN;
    header_to_write->biBitCount = BITCOUNT;
    header_to_write->biClrImportant = UNKNOWN;
    header_to_write->biClrUsed = UNKNOWN;
    header_to_write->biCompression = COMPRESSION;
    header_to_write->biHeight = y;
    header_to_write->biPlanes = PLANES;
    header_to_write->biSize = 40;
    header_to_write->biSizeImage = UNKNOWN;
    header_to_write->biWidth = x;
    header_to_write->biXPelsPerMeter = UNKNOWN;
    header_to_write->biYPelsPerMeter = UNKNOWN;
    header_to_write->bOffBits = sizeof(struct bmp_header);
}

enum read_status from_bmp(FILE *in, struct image *img)
{
    struct bmp_header bmp_header = {0};
    uint64_t check = fread(&bmp_header, sizeof(struct bmp_header), 1, in);
    if (check != 1)
    {
        return READ_INVALID_HEADER;
    }
    char check_result = check_bmp_header(&bmp_header);
    if (check_result != READ_OK)
    {
        return check_result;
    }
    
        create_image(img, bmp_header.biWidth, bmp_header.biHeight);
        uint32_t x = bmp_header.biWidth;
        uint32_t y = bmp_header.biHeight;
        uint64_t pad = padding(x);
        struct pixel *pixel = img->data;
        for (int i = 0; i < y - 1; i++)
        {
            uint64_t pread = fread(pixel, 1, (x * 3) + pad, in); 
            if (pread != (x * 3) + pad)
            {
                fclose(in);
                return READ_PIXELS_ERROR;
            }
            pixel = pixel + x; //ставим указатель на следующую строку
        }
        uint64_t pread = fread(pixel, 1, (x * 3), in); //читаем уже без паддинга, так как дошли до последней строки
                if (pread != (x * 3))
                {
                    fclose(in);
                    return READ_PIXELS_ERROR;
                }
        fclose(in);
        return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image const *img)
{
    uint64_t x = img->width;
    uint64_t pad_to_write = padding(x);
    struct pixel *pixel_to_write = img->data;
    struct bmp_header head = {0};
    make_header_to_write(img, &head);
    uint64_t is_write;
    is_write = fwrite(&head, sizeof(struct bmp_header), 1, out);
    if (is_write != 1)
    {   
        fclose(out);
        return WRITE_ERROR;
    }
    for (int i = 0; i < img->height; i++)
    {
        if (i == img->height-1)
        {
            is_write = fwrite(pixel_to_write, 1, (x * 3), out); 
            if (is_write != (x * 3))
            {
                fclose(out);
                return WRITE_ERROR;
            }
            break;
        }
        is_write = fwrite(pixel_to_write, 1, (x * 3) + pad_to_write, out); 
        if (is_write != (x * 3) + pad_to_write)
        {
            fclose(out);
            return WRITE_ERROR;
        }
        pixel_to_write = pixel_to_write + x; //ставим указатель на следующую строку
    }
    fclose(out);
    return WRITE_OK;
}
