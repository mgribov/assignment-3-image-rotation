#include "image.h"
#include <stdio.h>
struct __attribute__((packed)) bmp_header
{
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
};

struct bmp_header initHeader(int64_t width, int64_t height);

/*  deserializer   */
enum read_status  {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_COMPRESSION_UNSUPPORTED,
  READ_PLANES_UNSUPPORTED,
  READ_BITCOUNT_ERROR,
  READ_PIXELS_ERROR
  };

enum read_status from_bmp( FILE* in, struct image* img );

/*  serializer   */
enum  write_status  {
  WRITE_OK = 0,
  WRITE_ERROR
  /* коды других ошибок  */
};

uint64_t padding(uint64_t x);

enum write_status to_bmp( FILE* out, struct image const* img );
char * unpack_read_status(enum read_status status);
